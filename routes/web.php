<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'SMSController@index')->middleware('level:1');;
Route::get('/sms', [
    'as' => 'sms',
    'uses' => 'SMSController@show'
]);
Route::post('/sms', [
    'as' => 'sms.post',
    'uses' => 'SMSController@storePhoneNumber'
]);
Route::post('/custom', 'SMSController@sendCustomMessage');
Route::get( '/webhook', 'WebhookController@index' );
Route::get('/ocr',  [
    'as' => 'ocr',
    'uses' => 'OCRController@showForm'
]);
Route::post('/process', [
    'as' => 'ocr.process',
    'uses' => 'OCRController@submitForm'
]);
