@extends('layouts.app')

@section('content')
<div class="container">

    <div class="jumbotron">
        <h3>AWS Rekognition Service / OCR</h3>
        <p>This section demonstrates the extraction of text from any image.</p>
    </div>

    @if(session('success'))
        <div class="alert alert-success">
            <div class="form-group">{{ session('success') }}</div>
            <a href="/ocr" class="btn btn-success">Try Again</a>
        </div>
    @endif

    @if(isset($results))
        {{ dd($results) }}
    @else
        <form action="{{ action('OCRController@submitForm') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="type">Action</label>
                <select name="type" id="type" class="form-control">
                    <option value="text">Read Text</option>
                    <!--<option value="nudity">Detect Nudity</option>-->
                </select>
            </div>
            <div class="form-group">
                <label for="confidence">Minimum Confidence</label>
                <input type="number" id="confidence" name="confidence" class="form-control" value="50">
            </div>
            <div class="form-group">
                <label for="photo">Upload a Photo</label>
                <input type="file" name="photo" id="photo" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" value="Submit" class="btn btn-success btn-lg">
            </div>
        </form>
    @endif

</div>

@endsection
