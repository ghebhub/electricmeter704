@extends('layouts.app')

@section('content')
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> --}}
    <style>
        h4 {
            margin: 2rem 0rem 1rem;
        }

        .table-image {

            td,
            th {
                vertical-align: middle;
            }
        }
        .w-25 {
              height: 100px;
        }

    </style>

    <div class="container">
        <div class="jumbotron">
            <h3>TOWILIO SMS/MMS Gateway Service</h3>
            <p>This section demonstrates how to send SMS and recieve MMS through Towilio SMS gateway.</p>
        </div>
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Register A Meter With Phone Number
                        </div>
                        <div class="card-body">

                            <form method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Enter Related Phone Number To Meter</label>
                                    <input type="tel" class="form-control" name="phone_number"
                                        placeholder="Enter Phone Number">
                                </div>

                                <button type="submit" class="btn btn-primary">Register User</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Send SMS Message To Selected Meters
                        </div>
                        <div class="card-body">
                            <form method="POST" action="/custom">
                                @csrf
                                <div class="form-group">
                                    <label>Select Meters to notify</label>
                                    <select name="users[]" multiple class="form-control">
                                        @foreach ($users as $user)
                                            <option>{{ $user->phone_number }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Notification Message</label>
                                    <textarea name="body" class="form-control" rows="3" placeholder="Photo Shooting
Edit the SMS «000» and send to the product A photo will be automatically taken and saved in the TF card.
Reply: The Photo has been saved in the memory card.

Photo Shooting and Sending Back
Edit the SMS «111» and send to the product (A card with GPRS and colored SMS is required for this function )

Video Recording
Edit the SMS «222» and send to the product (Send «444» during video recording will end the process and save to the TF card.)
Reply: Start recording video

Audio Recording
Edit the SMS «333» and send to the product (Send «444» during audio recording will end the process and save to the TF card.)
Reply: Start recording sound

Memory Card Formatting
Edit the SMS «999» and send to the product, the memory card will be formatted.

Sound Control Callback
• Message Callback
The authorized person edits the SMS «777» and send to the product. When the sound around the product exceeds 60 dB. there will be a Chinese SMS «Safety tips: Someone broke into secretly» replied to the authorized person.

• Phone Callback
The authorized person edits the SMS «888» and send to the product. When the sound around the product exceeds 60 dB. there will be a direct dial-back to the mobile phone of the authorized person.

Battery Status:
Edit the SMS «666» and send to the product, you can check the status of the battery."></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Send Notification</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card-header">
                        SMS/MMS Received from registered Meters | Try to Send MMS to this number <a
                            href="sms:+16206046747">+16206046747</a> it should list below.
                    </div>
                    <div class="card">

                        <table class="table table-image table-striped">
                            <thead>
                                <tr>
                                <tr>
                                    <th scope="col">MessageId</th>
                                    <th scope="col">Meter #</th>
                                    <th scope="col">Body</th>
                                    <th scope="col">Image</th>
                                </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allSms as $sms)
                                    <tr>
                                        <th scope="row">{{ \Illuminate\Support\Str::limit($sms->MessageSid, 6, $end='...') }}</th>
                                        <td>{{ $sms->fromNumber }}</td>
                                        <td>{{ \Illuminate\Support\Str::limit($sms->body, 6, $end='...') }}</td>
                                        <td >
                                            <img src="{{ $sms->mediaUrl }}" class="img-fluid img-thumbnail"
                                                alt="{{ $sms->mediaUrl }}" class="w-25">
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
