<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\MMSMedia;
use GuzzleHttp\Client as HttpClient;
use App\Services\MediaMessageService\IMediaMessageService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Magyarjeti\MimeTypes\MimeTypeConverter;
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
// use Twilio\TwiML;
class MessagingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Messaging Controller
    |--------------------------------------------------------------------------
    |
    | This controller receives messages from Twilio and makes the media available
    | via the /images url.
    */

    protected $twilio;
    protected $accountSid;
    protected $twilioNumber;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->accountSid = env('TWILIO_SID');
        $this->twilioNumber = env('TWILIO_NUMBER');
        $authToken = env('TWILIO_AUTH_TOKEN');

        $this->twilio = new Client($this->accountSid, $authToken);
    }

    public function handleIncomingSMS(Request $request, IMediaMessageService $mediaService)
    {
        $converter = new MimeTypeConverter;
        $NumMedia = (int)$request->input('NumMedia');
        $fromNumber = $request->input('From');
        $toNumber = $request->input('To');
        $body = $request->input('Body');
        $Sid = $request->input('AccountSid');
        $MessageSid = $request->input('MessageSid');
        if($NumMedia > 0){
              for ($i=0; $i < $NumMedia; $i++) {
                $mediaUrl = $request->input("MediaUrl$i");
                $MIMEType = $request->input("MediaContentType$i");
                $fileExtension = $converter->toExtension($MIMEType);

                $mediaSid = basename($mediaUrl);

                $media = $mediaService->getMediaContent($mediaUrl);

                $filename = "$mediaSid.$fileExtension";

                $mediaData = compact('fromNumber', 'toNumber', 'accountSid', 'body','mediaSid', 'MessageSid', 'mediaUrl', 'media', 'filename', 'MIMEType');
                $mmsMedia = new MMSMedia($mediaData);
                $mmsMedia->save();
               // anyLineOCRSerivce(base64_encode($media));
            }
        }else{
                $mediaData = compact('fromNumber', 'toNumber', 'accountSid', 'body','mediaSid', 'MessageSid', 'mediaUrl', 'media', 'filename', 'MIMEType');
                $mmsMedia = new MMSMedia($mediaData);
                $mmsMedia->save();
        }


        $response = new MessagingResponse();
        $messageBody = $NumMedia == 0 ? 'Send us an image!' : "Thanks for the $NumMedia images.";
        $response->message($messageBody);
        return response($response);
    }

    public function deleteMediaFromTwilio($mediaItem)
    {
        return $this->twilio->api->accounts($this->accountSid)
            ->messages($mediaItem['MessageSid'])
            ->media($mediaItem['mediaSid'])
            ->delete();
    }

    public function allMedia()
    {
        $mediaItems = MMSMedia::all();
        return $mediaItems;
    }

    public function getMediaFile($filename, Response $response)
    {
        $media = MMSMedia::where('filename', $filename)->firstOrFail();
        $fileContents = $media['media'];
        $MessageSid = $media['MessageSid'];
        $mediaSid = $media['mediaSid'];
        $MIMEType = $media['MIMEType'];

        $media->delete();
        $this->deleteMediaFromTwilio(compact('mediaSid', 'MessageSid'));

        return response($fileContents, 200)
            ->header('Content-Type', $MIMEType);
    }

    public function config()
    {
        return ['twilioNumber' => $this->twilioNumber];
    }

    public function anyLineOCRSerivce(Request $request)
    {
        $base64Image = $request->input('base64Image');
        $url = env("ANY_LINE_API_URL");

        $params = [
            //If you have any Params Pass here
        ];

        $headers = [
            'Content-Type'=>'application/json',
            'Accept'=>'application/json'
        ];

        $response = array();
        try {
            $bodyArr= array (
                'config' =>
                array (
                  'viewPlugin' =>
                  array (
                    'plugin' =>
                    array (
                      'meterPlugin' =>
                      array (
                        'scanMode' => 'AUTO_ANALOG_DIGITAL_METER',
                      ),
                    ),
                  ),
                ),
                'license' => env('ANYLINE_LIC'),
                'blob' => $base64Image,
            );
            $body = json_encode($bodyArr);
            $options = [$headers, $body];
            $client = new HttpClient();
            // $response = $client->request('POST', $url, $options);
            $client = new \GuzzleHttp\Client();
            $promise = $client->requestAsync('POST', $url, $options);
            $promise->then(
                function (ResponseInterface $res) {
                    echo $res->getStatusCode() . "\n";
                },
                function (RequestException $e) {
                    echo $e->getMessage() . "\n";
                    echo $e->getRequest()->getMethod();
                }
            );
            $response->getBody();
        } catch (\Exception $e) {
            abort(503);
        }

        // if ($response->status() == 401 && !request()->routeIs('login')) {
        //     throw new AuthenticationException();
        // }
         $responseBody = json_decode($response->getBody());
         return $responseBody;

    }

    public function parseImageFile($imgFile, $options = [])
    {
        return $this->parseImage('file', fopen($imgFile, 'r'), $options);
    }

    public function parseImageUrl($imgUrl, $options = [])
    {
        return $this->parseImage('url', $imgUrl, $options);
    }

    public function parseImageBinary($imgBinary, $mimeType, $options = [])
    {
        return $this->parseImageBase64(base64_encode($imgBinary), $mimeType, $options);
    }

    public function parseImageBase64($imgBase64, $mimeType, $options = [])
    {
        return $this->parseImage('base64Image', 'data:' . $mimeType . ';base64,' . $imgBase64, $options);
    }

    protected function parseImage($fldName, $fldValue, $options = [])
    {
        $client = new HttpClient();

        $lang = isset($options['lang']) ? $options['lang'] : 'eng';
        $headers = [ 'apikey' => $this->key ];
        $multipart = [
                [ 'name' => 'language', 'contents' => 'eng' ],
                [ 'name' => $fldName, 'contents' => $fldValue ]
            ];

        $url = $this->url == '' ? 'https://api.ocr.space/parse/image' : $this->url;
        try {
            $response = $client->request('POST', $url, ['headers' => $headers, 'multipart' => $multipart]);
        } catch (\Exception $e) {
            throw new OcrException('Error connecting to service URL: ' . $url, 0, $e);
        }

        $code = $response->getStatusCode();
        if ($code != 200) {
            throw new OcrException('HTTP error returned from service URL: ' . $url, $code);
        }
        $body = $response->getBody();
        return new OcrResponse(json_decode($body));
    }
}
