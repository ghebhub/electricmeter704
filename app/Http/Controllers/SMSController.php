<?php

namespace App\Http\Controllers;

use App\UsersPhoneNumber;
use App\MMSMedia;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class SMSController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Show the forms with users phone number details.
     *
     * @return Response
     */
    public function show()
    {
        $users = UsersPhoneNumber::all();
        $allSms = MMSMedia::all();
        return view('sms', compact("users","allSms"));
    }

    /**
     * Store a new user phone number.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storePhoneNumber(Request $request)
    {
        //run validation on data sent in
        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users_phone_number|numeric',
        ]);

        $user_phone_number_model = new UsersPhoneNumber($request->all());
        $user_phone_number_model->save();
        $this->sendMessage('User registration successfull!!', $request->phone_number);
        return back()->with(['success' => "{$request->phone_number} registered"]);
    }

    /**
     * Send message to a selected users
     */
    public function sendCustomMessage(Request $request)
    {
        $validatedData = $request->validate([
            'users' => 'required|array',
            'body' => 'required',
        ]);
        $recipients = $validatedData["users"];
        $messageResponse="";
        // iterate over the arrray of recipients and send a twilio request for each
        foreach ($recipients as $recipient) {
            $responseSid = $this->sendMessage($validatedData["body"], $recipient);
            $messageResponse = $messageResponse." : ".$responseSid;
        }
        return back()->with(['success' => "Messages on their way!".$messageResponse]);
    }

    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param Number $recipients Number of recepient
     */
    private function sendMessage($message, $recipients)
    {
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");

        $client = new Client($account_sid, $auth_token);
        $response = $client->messages->create($recipients, array('from' => $twilio_number, 'body' => $message));
        return   $response->sid;
    }
}
