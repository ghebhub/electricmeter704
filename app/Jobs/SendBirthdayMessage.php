<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SendBirthdayMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $twilioClient;

    /**
     * Create a new job instance.
     * @param User $user: A single user instance whose birthday is current date.
     * @throws ConfigurationException
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $this->twilioClient = new Client($sid, $token);
    }

    /**
     * Execute the job.
     * @throws TwilioException
     * @return void
     */
    public function handle()
    {
        $template = "You're finally old enough to know better! Happy Birthday %s";
        $body = sprintf($template, $this->user->name);
        $message = $this->twilioClient->messages->create(
            $this->user->phone,
            [
                'from' => config('services.twilio.phone_number'),
                'body' => $body
            ]
        );
        Log::info($message->sid);
    }
}