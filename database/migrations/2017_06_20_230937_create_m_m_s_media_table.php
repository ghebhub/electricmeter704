<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMMSMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_m_s_media', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fromNumber')->index();
            $table->string('toNumber')->index();
            $table->string('accountSid')->nullable();
            $table->string('body')->nullable();
            $table->string('mediaSid')->nullable();
            $table->string('MessageSid')->nullable();
            $table->string('mediaUrl')->nullable();
            $table->binary('media')->nullable();
            $table->string('filename')->nullable();
            $table->string('MIMEType')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_m_s_media');
    }
}
